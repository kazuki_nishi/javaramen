/**
* ラーメン・クラス
*/
public class Ramen {
    /**
    * ラーメンのスープ
    */
    private String fSoup = null;

    /**
    * ラーメン・クラスのコンストラクタ
    */
    public Ramen() {
        // スープに「とんこつ」をセット
        fSoup = "とんこつ";
    }
    /**
    * ラーメンを表示
    */
    public void showRamen() {
        System.out.println("ラーメンを表示");
        System.out.println(" スープ : " + getSoup());
    }
    /**
    * ラーメンのスープを取得
    *
    * @return スープ
    */
    public String getSoup() {
        return fSoup;
    }
}